if (typeof window !== "undefined") {
    window.addEventListener("load", function () {

        let currentChoice1, currentChoice2;
        let number1 = prompt("Please enter the first number");
        while (!number1 || isNaN(+number1) || number1 === "") {
            currentChoice1 = number1
            number1 = prompt("Please re-enter the first number", `Previously you've entered ${currentChoice1}`);
        }
        let number2 = prompt("Please enter the second number");
        while (!number2 || isNaN(+number2) || number2 === "") {
            currentChoice2 = number2
            number2 = prompt("Please re-enter the second number", `Previously you've entered ${currentChoice2}`);
        }
        let operationSign = prompt("Please enter operation sign. Signs available - +, -, *, /")

        makeOperation(+number1, +number2, operationSign);


        function makeOperation(num1, num2, operationSign) {
            let result;
            if (operationSign === "+") {
                result = num1 + num2;
                console.log(`you chose +, result ${num1} + ${num2} = ${result}`);
            } else if (operationSign === "-") {
                result = num1 - num2;
                console.log(`you chose -, result ${num1} - ${num2} = ${result}`);
            } else if (operationSign === "*") {
                result = num1 * num2;
                console.log(`you chose *, result ${num1} * ${num2} = ${result}`);
            } else if (operationSign === "/") {
                if (num2 === 0) {
                    console.log("The second operand is 0. You can not divide by zero")
                } else {
                    result = num1 / num2;
                    console.log(`you chose /, result ${num1} / ${num2} = ${result}`);
                }
            }
            else {
                console.log("You have entered wrong operation sign")
            }
        }
    });
}
